import { Component } from '@angular/core';

@Component({
  selector: 'app-carrito-c',
  templateUrl: './carrito-c.component.html',
  styleUrls: ['./carrito-c.component.css']
})

export class CarritoCComponent {

  productos = [
    { nombre: 'Producto 1', precio: 10 },
    { nombre: 'Producto 2', precio: 15 },
    { nombre: 'Producto 3', precio: 20 }
  ];

  agregarProducto() {
    const nuevoProducto = { nombre: 'Producto Nuevo', precio: 25 };
    this.productos.push(nuevoProducto);
  }

  eliminarProducto(producto: any) {
    const index = this.productos.indexOf(producto);
    if (index !== -1) {
      this.productos.splice(index, 1);
    }

}
calcularSubtotal(): number {
  let subtotal = 0;
  for (let producto of this.productos) {
    subtotal += producto.precio;
  }
  return subtotal;
}

calcularTotal(): number {
  const subtotal = this.calcularSubtotal();
  const impuesto = subtotal * 0.1; // Supongamos un impuesto del 10%.
  const total = subtotal + impuesto;
  return total;
}
}
